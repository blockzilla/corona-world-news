
const forge = require('node-forge');
const NewsAPI = require('newsapi');
var TinyURL = require('tinyurl');
const Tortoise = require('tortoise')
const redis = require('redis');
const db1 = require('./db')
const botQueue = process.env.MESSAGE_QUEUE || 'job-log';
const newsQueue = process.env.MESSAGE_QUEUE || 'news';

// initialise redis
const redisClient = redis.createClient(process.env.REDIS_PORT || '6379', process.env.REDIS_HOST || 'localhost');
redisClient.on('connect', function () {
    console.log('Redis redisClient connected');
});
redisClient.on('error', function (err) {
    console.log('Something went wrong ' + err);
});

db1.createTables()

// initialise queues
const tortoise = new Tortoise(process.env.RABBIT_HOST || 'amqp://guest:guest@127.0.0.1:5672/')
tortoise.on(Tortoise.EVENTS.CONNECTIONCLOSED, () => {
    console.log('RabbitMQ connection closed')
})
tortoise.on(Tortoise.EVENTS.CONNECTIONDISCONNECTED, () => {
    console.log('RabbitMQ connection disconnected')
})
tortoise.on(Tortoise.EVENTS.CONNECTIONERROR, (err) => {
    console.log('RabbitMQ connection error', err)
})

// news api
const newsapiKey = process.env.NEWS_API_KEY || '';
const newsapi = new NewsAPI(newsapiKey);

var toDate = new Date()
var fromDate = new Date();
fromDate.setDate(fromDate.getDate() - 1);

// hold a new object
var newsObj = {
    "hash": "",
    "news": []
}

// create a hash of the the input string
function hashCode(s) {
    var md = forge.md.sha1.create();
    md.update(s);
    return md.digest().toHex();
}

// call news api and update database
function getNews() {
    console.log('Calling get news API');
    newsapi.v2.everything({
        q: 'coronavirus, covid-19',
        language: 'en',
        sortBy: 'relevancy',
        from: fromDate.toISOString().slice(0, 10),
        to: toDate.toISOString().slice(0, 10),
        noCache: true
    }).then(response => {
        if (response !== null && response.articles.length) {
            // news = response.articles;
            var msgIndex = 0;
            response.articles.forEach(function (item, index) {
                console.log(item)
                const newsHash = hashCode(
                    item.title.toLowerCase()
                        .substring(0, 19)
                        .trim()
                );
                item.publishedAt = new Date(item.publishedAt).toLocaleString()
                try {
                    db1.query('SELECT id FROM news WHERE id = $1', [newsHash], (err, res) => {
                        msgIndex++;
                        if (err) {
                            console.log(err.stack)
                        } else {
                            console.log(res.rows)
                            if (res.rows[0]) {
                                console.log('Matching Hash Found, drop duplicate message')
                            } else {
                                uniqueNews(item, newsHash);
                            }
                            if (response.articles.length === msgIndex) {
                                setTimeout(function () {
                                    console.log('Shutting down application')
                                    process.exit(0);
                                }, 12000);
                            }
                        }
                    })
                } catch (err) {
                    console.log('Error reading news', err.message);
                    process.exit(0);
                }
            });
        }
    });
    return;
}

// Update the news newsItem in the database and continue
function uniqueNews(newsItem, newsHash) {
    let latest = []
    var strNews = JSON.stringify(newsItem)
    latest.push(newsItem);

    try {
        db1.query('INSERT INTO news(id, json, date) VALUES($1, $2, $3)', [newsHash, strNews, new Date(newsItem.publishedAt).getTime()], (err, res) => {
            if (err) {
                console.log(err.stack)
            } else {
                console.log(
                    `A row has been inserted with value ${newsHash}`
                );
                buildTelegramBotMessage(latest);
                updateNewsCache();
            }
        })
    } catch (err) {
        console.log('Error reading news', err.message);
    }
    return;
}

// push message to queue
function pushToQueue(msg, queue) {
    console.log("pushing news item to queue")
    tortoise
        .queue(queue, { durable: false })
        .publish(msg)
        .then(() => {
            console.log('News update received, published message to queue', {
                queue,
                msg
            })
        })
        .catch((err) => {
            console.log('New update received, error happened during message publish to queue', {
                queue,
                err
            })
            throw err
        })
    return;
}

// build a message for telegram queue
function buildTelegramBotMessage(data) {
    data = data[0];
    var msg = { 'message': '' };
    var tmp = data.title + ' \n';
    console.log("Building Telegram message")
    // shorten URLs
    TinyURL.shorten(data.url, function (res, err) {
        if (err) {
            console.log(err)
        }
        msg.message = tmp + res;
        pushToQueue(msg, botQueue);
    });
    return;
}


function sortter(a, b) {
    const publishedAtA = a.publishedAt.toUpperCase();
    const publishedAtB = b.publishedAt.toUpperCase();
    let comparison = 0;
    if (publishedAtA > publishedAtB) {
        comparison = 1;
    } else if (publishedAtA < publishedAtB) {
        comparison = -1;
    }
    return comparison;
}

// query the DB and update redis
function updateNewsCache() {
    console.log("reading news");
    try {
        db1.query('SELECT json FROM news ORDER BY date DESC', (err, res) => {
            if (err) {
                console.log(err.stack)
            } else {
                res.rows.forEach(row => {
                    var newData = {};
                    newData = JSON.parse(row.json);
                    newsObj.news.push(newData);
                })
                newsObj.hash = hashCode(JSON.stringify(newsObj.news))
                redisClient.set('news', JSON.stringify(newsObj), redis.print);
                pushToQueue({}, newsQueue);
                console.log('updating cache')
            }
        })
    } catch (err) {
        console.log('Retrying', err.message);
    }
    return;
}

// update data
updateNewsCache();
getNews();

setTimeout(function () {
    process.exit(0);
}, 60000);



console.log('version 0.0.1')